#include "../include/student_database.h"
#include "../include/sort.h"

Student *database = NULL;
int db_size = 0;
int max_id = 0;

int write_database(char *filename_P) {
    FILE *fp = fopen(filename_P, "w");
    if (fp == NULL) {
        printf("Error. Unable to write database file\n");
        return F_WRITE_ERROR;
    }
    for (int i = 0; i < db_size; i++) {
        fprintf(fp, "%d:%s:%d:%f:%s:", database[i].id, database[i].name, database[i].student_number,
                database[i].grade_average,
                database[i].login);
        for (int j = 0; j < HASH_SIZE; j++) {
            fprintf(fp, "%02x", database[i].password_hash[j]);
        }
        fprintf(fp, "\n");
    }
    fclose(fp);
}

void add_student(char *filename_P) {
    char name[NAME_MAX_LENGTH], login[LOGIN_MAX_LENGTH];
    int student_number;
    int id = ++max_id;
    float gpa;
    printf("Enter name: ");
    scanf("%s", name);
    printf("Enter student number: ");
    scanf("%d", &student_number);
    printf("Enter average point: ");
    scanf("%f", &gpa);
    printf("Enter login: ");
    scanf("%s", login);
    char password[LOGIN_MAX_LENGTH];
    printf("Enter password: ");
    scanf("%s", password);
    uint8_t password_hash[HASH_SIZE];
    calculate_sha(password_hash, password, strlen(password));
    memset(password, 0, strlen(password)); // clear password from memory

    // Check if ID, login, and student_number are unique
    for (int i = 0; i < db_size; i++) {
        if (database[i].id == id) {
            printf("Error. ID already exists\n");
            return;
        }
        if (strcmp(database[i].login, login) == 0) {
            printf("Error. Login already exists\n");
            return;
        }
        if (database[i].student_number == student_number) {
            printf("Error. Student number already exists\n");
            return;
        }
    }

    Student student = {id, "", student_number, gpa, "", {0}};
    strncpy(student.name, name, NAME_MAX_LENGTH);
    strncpy(student.login, login, LOGIN_MAX_LENGTH);
    memcpy(student.password_hash, password_hash, HASH_SIZE);
    database = realloc(database, (db_size + 1) * sizeof(Student));
    database[db_size] = student;
    db_size++;
    write_database(filename_P);
    printf("\n\n\n");
}

void delete_student(char *filename_P) {
    int id;
    printf("Enter ID of student to delete him from DB: ");
    scanf("%d", &id);
    for (int i = 0; i < db_size; i++) {
        if (database[i].id == id) {
            memmove(&database[i], &database[i + 1], (db_size - i - 1) * sizeof(Student));
            db_size--;
            write_database(filename_P);
            printf("\n\n\n");
            return;
        }
    }
    printf("Error. Student with ID %d not found\n\n\n\n", id);
}

void menu() {
    printf("1. Add student\n");
    printf("2. View all students\n");
    printf("3. Delete student by ID\n");
    printf("4. View rating\n");
    printf("5. Authenticate student\n");
    printf("0. Exit\n\n\n");
}

void view() {
    printf("ID\t\tName\t\tCard number\t\tAverage point\t\tLogin\n");
    for (int i = 0; i < db_size; i++) {
        printf("%d\t\t%s\t\t%d\t\t\t%.2f\t\t\t%s\n", database[i].id, database[i].name, database[i].student_number,
               database[i].grade_average, database[i].login);
    }
    printf("\n\n\n");
}

void output_rating(Student *students_P, int num_students) {
    merge_sort(students_P, 0, num_students - 1);

    printf("ID\t\tName\t\tCard number\t\tAvreage point\t\tLogin\n");
    for (int i = 0; i < num_students; i++) {
        printf("%d\t\t%s\t\t%d\t\t\t%.2f\t\t\t\t%s\n", students_P[i].id, students_P[i].name, students_P[i].student_number,
               students_P[i].grade_average, students_P[i].login);
    }
    printf("\n\n\n");
}

int read_database(char *filename_P) {
    FILE *fp = fopen(filename_P, "r");
    if (fp == NULL) {
        printf("We can't read db :( please call your programming men\n");
        return F_READ_ERROR;
    }
    int id, student_number;
    float gpa;
    char name[NAME_MAX_LENGTH], login[LOGIN_MAX_LENGTH], password_hash_str[2 * HASH_SIZE + 1];
    while (fscanf(fp, "%d:%[^:]:%d:%f:%[^:]:%s\n", &id, name, &student_number, &gpa, login, password_hash_str) == 6) {
        uint8_t password_hash[HASH_SIZE];
        for (int i = 0; i < HASH_SIZE; i++) {
            sscanf(&password_hash_str[2 * i], "%2hhx", &password_hash[i]);
        }
        Student student = {id, "", student_number, gpa, "", {0}};
        strncpy(student.name, name, NAME_MAX_LENGTH);
        strncpy(student.login, login, LOGIN_MAX_LENGTH);
        memcpy(student.password_hash, password_hash, HASH_SIZE);
        database = realloc(database, (db_size + 1) * sizeof(Student));
        database[db_size] = student;
        if (student.id > max_id)
            max_id = student.id;
        db_size++;
    }
    fclose(fp);
}
#include "../include/sort.h"

void merge(Student *students_P, int left, int middle, int right) {
    int i, j, k;
    int left_size = middle - left + 1;
    int right_size = right - middle;

    Student left_array[left_size], right_array[right_size];

    for (i = 0; i < left_size; i++) {
        left_array[i] = students_P[left + i];
    }
    for (j = 0; j < right_size; j++) {
        right_array[j] = students_P[middle + 1 + j];
    }

    i = 0;
    j = 0;
    k = left;
    while (i < left_size && j < right_size) {
        if (left_array[i].grade_average >= right_array[j].grade_average) {
            students_P[k] = left_array[i];
            i++;
        } else {
            students_P[k] = right_array[j];
            j++;
        }
        k++;
    }

    while (i < left_size) {
        students_P[k] = left_array[i];
        i++;
        k++;
    }

    while (j < right_size) {
        students_P[k] = right_array[j];
        j++;
        k++;
    }
}

void merge_sort(Student *students_P, int left, int right) {
    if (left < right) {
        int middle = left + (right - left) / 2;
        merge_sort(students_P, left, middle);
        merge_sort(students_P, middle + 1, right);
        merge(students_P, left, middle, right);
    }
}

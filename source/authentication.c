#include "../include/authentication.h"

bool authentication(char *login_P, char *password_P) {
    for (int i = 0; i < db_size; i++) {
        if (strcmp(database[i].login, login_P) == 0) {
            uint8_t pass_hash[HASH_SIZE];
            calculate_sha(pass_hash, password_P, HASH_SIZE);
            if (memcmp(pass_hash, database[i].password_hash, HASH_SIZE) == 0) {
                memset(password_P, 0, strlen(password_P));
                return true;
            } else {
                memset(password_P, 0, strlen(password_P));
                return false;
            }
        }
    }
    memset(password_P, 0, strlen(password_P));
    return false;
}

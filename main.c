#include "include/student_database.h"

#define INPUT_ERROR 1

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: %s <filename>\n", argv[0]);
        return INPUT_ERROR;
    }

    read_database(argv[1]);
    int choice;
    do {
        menu();
        printf("Enter choice: ");
        scanf("%d", &choice);
        switch (choice) {
            case ADD:
                add_student(argv[1]);
                break;
            case VIEW:
                view();
                break;
            case DELETE:
                delete_student(argv[1]);
                break;
            case RATING:
                output_rating(database, db_size);
                break;
            case AUTHENTICATE: {
                char login[LOGIN_MAX_LENGTH], password[LOGIN_MAX_LENGTH];
                printf("Enter login: ");
                scanf("%s", login);
                printf("Enter password: ");
                scanf("%s", password);
                printf("\n\n");
                if (authentication(login, password)) {
                    printf("Authentication successful\n\n\n\n");
                } else {
                    printf("Authentication failed\n\n\n\n");
                }
                break;
            }
            case EXIT:
                break;
            default:
                printf("Error. Invalid choice\n\n\n\n");
                break;
        }
    } while (choice != 0);
    free(database);
    return 0;
}

#ifndef INC_5_3_SORT_H
#define INC_5_3_SORT_H

#include "student_database.h"

void merge(Student *students_P, int left, int middle, int right);

void merge_sort(Student *students_P, int left, int right);

#endif

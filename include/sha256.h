#ifndef SHA_256_H
#define SHA_256_H

#include <stdint.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif


#define HASH_SIZE 32
#define CHUNK_SIZE 64

struct Sha_256 {
    uint8_t *hash;
    uint8_t chunk[CHUNK_SIZE];
    uint8_t *chunk_pos;
    size_t space_left;
    size_t total_len;
    uint32_t h[8];
};


void calculate_sha(uint8_t hash[HASH_SIZE], const void *input, size_t len);

void sha_init(struct Sha_256 *sha_256_P, uint8_t hash[HASH_SIZE]);

void write_sha(struct Sha_256 *sha_256_P, const void *data, size_t len);


uint8_t *sha_256_close(struct Sha_256 *sha_256_P);

#ifdef __cplusplus
}
#endif

#endif

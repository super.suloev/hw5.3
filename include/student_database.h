#ifndef INC_5_3_STUDENT_DATABASE_H
#define INC_5_3_STUDENT_DATABASE_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "sha256.h"
#include "authentication.h"

#define SIZE_OF_SHA_256_HASH 32
#define NAME_MAX_LENGTH 50
#define LOGIN_MAX_LENGTH 20

#define F_WRITE_ERROR 1
#define F_READ_ERROR 2

typedef struct {
    int id;
    char name[NAME_MAX_LENGTH];
    int student_number;
    float grade_average;
    char login[LOGIN_MAX_LENGTH];
    uint8_t password_hash[HASH_SIZE];
} Student;

extern Student *database;
extern int db_size;
extern int max_id;

enum choice {
    EXIT = 0,
    ADD,
    VIEW,
    DELETE,
    RATING,
    AUTHENTICATE
};

int write_database(char *filename_P);

void add_student(char *filename_P);

void delete_student(char *filename_P);

void menu();

void view();

void output_rating(Student *students_P, int num_students);

int read_database(char *filename_P);

#endif //INC_5_3_STUDENT_DATABASE_H
